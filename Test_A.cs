﻿using System;

namespace ConsoleApp20220625
{
    public class MainClass
    {
        public static void Main(string[] args)
        {
            int countTestInput = int.Parse(Console.ReadLine());
            do
            {
                string[] stringNumbers = Console.ReadLine().Split(' ');
                int[] numbers = new int[stringNumbers.Length];

                for (int i = 0; i < stringNumbers.Length; i++)
                    numbers[i] = int.Parse(stringNumbers[i]);

                Console.WriteLine(GetSumm(numbers));

                countTestInput--;
            }
            while (countTestInput >= 1);

            static int GetSumm(int[] numbers)
            {
                int result = 0;
                foreach (var n in numbers)
                    result += n;
                return result;
            }

        }
    }
}
